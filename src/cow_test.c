#include "cow.h"
#include "heap.h"
#include <assert.h>

typedef struct {
    uint64_t value0;
    uint64_t value1;
    uint32_t value2;
    uint32_t value3;
    uint8_t value4;
    uint8_t value4a;
    uint8_t value4b;
    uint16_t value5;
} aligned_t;

void
test_align()
{
    heap_t *heap = heap_init(2048);
    cow_t* cow = cow_init(heap, 100);
    aligned_t x;

    cow_write_uint64_t(cow, &x.value0, 0xDEADBEEFDEADBEEF);
    cow_write_uint64_t(cow, &x.value1, 0xBEEFBEEFBAC0B1C0);
    cow_write_uint32_t(cow, &x.value2, 0xDEADBAC0);
    cow_write_uint32_t(cow, &x.value3, 0xBBBBAAAA);
    cow_write_uint8_t(cow, &x.value4, 0xCC);
    cow_write_uint8_t(cow, &x.value4a, 0xCD);
    cow_write_uint8_t(cow, &x.value4b, 0xCE);
    cow_write_uint16_t(cow, &x.value5, 0xDDDD);

    cow_apply(cow);

    assert(x.value0 == 0xDEADBEEFDEADBEEF);
    assert(x.value1 == 0xBEEFBEEFBAC0B1C0);
    assert(x.value2 == 0xDEADBAC0);
    assert(x.value3 == 0xBBBBAAAA);
    assert(x.value4 == 0xCC);
    assert(x.value4a == 0xCD);
    assert(x.value4b == 0xCE);
    assert(x.value5 == 0xDDDD);

    cow_fini(cow);

    heap_fini(heap);
}

void
test_chars() {
//    heap_t *heap = heap_init(2048);
//    cow_t *cow = cow_init(heap, 100);
//
//    char chars[2];
//    cow_write_uint8_t(cow,
//    [0] = 'b';
//    buf[1] = '\0';
//    printf_s("buffer content: %s\n", buf);
}


int
main(int argc, char* argv[])
{
    // no args, run all tests
    if (argc == 1) {
        test_align();
        test_chars();
        // only run requested test
    } else {
        switch (atoi(argv[1])) {
        case 0:
            test_align();
            break;
        case 1:
            test_chars();
            break;
        default:
            return -1;
        }
    }

    return 0;
}
